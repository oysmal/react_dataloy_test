import { createStore, combineReducers } from 'redux';
import { vessels } from './vessels/vessels';
import { voyages } from './voyages/voyages';
import { visibilityFilter } from './visibility-filter/visibility_filter';
import { VESSEL_FILTERS } from './visibility-filter/vessel_filters';
import { VOYAGE_FILTERS } from './visibility-filter/voyage_filters';

const initialStore = {
    vessels: [],
    voyages: [],
    visibilityFilter: {
        vessels: VESSEL_FILTERS.ALL,
        voyages: VOYAGE_FILTERS.ALL,
    }
}

export const createFleetPlanStore = () => {
    return createStore(combineReducers({
        vessels,
        voyages,
        visibilityFilter
    }), initialStore, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
}

