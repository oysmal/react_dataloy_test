import {VISIBILITY_FILTER_ACTIONS} from './visibility_filter';

export const set_vessel_visibility_filter_action = (vessel_types_to_show) => {
    return {
        type: VISIBILITY_FILTER_ACTIONS.SET_VESSEL_VISIBILITY_FILTER,
        vessel_types_to_show: vessel_types_to_show
    };
}

export const set_voyage_visibility_filter_action = (voyage_types_to_show) => {
    return {
        type: VISIBILITY_FILTER_ACTIONS.SET_VOYAGE_VISIBILITY_FILTER,
        voyage_types_to_show: voyage_types_to_show
    };
}