import {VESSEL_FILTERS} from './vessel_filters';
import {VOYAGE_FILTERS} from './voyage_filters';

export const VISIBILITY_FILTER_ACTIONS = {
    SET_VESSEL_VISIBILITY_FILTER: 'visibility_filter.set_vessel_visibility_filter',
    SET_VOYAGE_VISIBILITY_FILTER: 'visibility_filter.set_voyage_visibility_filter'
}

export const visibilityFilter = (visibilityFilter, action) => {
    if(!visibilityFilter) {
        return {
            vessels: VESSEL_FILTERS.ALL,
            voyages: VOYAGE_FILTERS.ALL,
        };
    }

    switch(action.type) {
        case VISIBILITY_FILTER_ACTIONS.SET_VESSEL_VISIBILITY_FILTER:
            return set_vessel_visibility_filter(visibilityFilter, action);
        case VISIBILITY_FILTER_ACTIONS.SET_VOYAGE_VISIBILITY_FILTER:
            return set_voyage_visibility_filter(visibilityFilter, action);
        default:
            return visibilityFilter;
    }
}

const set_vessel_visibility_filter = (visibilityFilter, action) => {
    return Object.assign({}, visibilityFilter, {vessels: action.vessel_types_to_show})
}

const set_voyage_visibility_filter = (visibilityFilter, action) => {
    return Object.assign({}, visibilityFilter, {voyages: action.voyage_types_to_show})
}