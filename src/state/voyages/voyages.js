export const VOYAGE_ACTIONS = {
    UPDATE_VOYAGES: 'voyages.update_voyages',
    UPDATE_VOYAGE: 'voyages.update_voyage'
}

export const voyages = (voyages, action) => {
    if(!voyages) return [];

    switch(action.type) {
        case VOYAGE_ACTIONS.UPDATE_VOYAGE:
            return update_voyage(voyages, action);
        case VOYAGE_ACTIONS.UPDATE_VOYAGES:
            return update_voyages(voyages, action);
        default:
            return voyages;
    }
}

const update_voyage = (voyages, action) => {
    return voyages
    .slice(0, action.index)
    .concat([action.voyage])
    .concat(voyages.slice(action.index+1))
}

const update_voyages = (voyages, action) => {
    return action.voyages;
}

