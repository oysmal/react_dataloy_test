import {VOYAGE_ACTIONS} from './voyages';

export const update_voyage_action = (voyage, index) => {
    return {
        type: VOYAGE_ACTIONS.UPDATE_VOYAGES,
        voyage: voyage,
        index: index
    };
}

export const update_voyages_action = (voyages) => {
    return {
        type: VOYAGE_ACTIONS.UPDATE_VOYAGES,
        voyages: voyages
    };
}