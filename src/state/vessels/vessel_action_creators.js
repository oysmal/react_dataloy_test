import {VESSEL_ACTIONS} from './vessels';

export const update_vessel_action = (vessel, index) => {
    return {
        type: VESSEL_ACTIONS.UPDATE_VESSEL,
        vessel: vessel,
        index: index
    };
}

export const update_vessels_action = (vessels) => {
    return {
        type: VESSEL_ACTIONS.UPDATE_VESSELS,
        vessels: vessels
    };
}