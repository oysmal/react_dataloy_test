export const VESSEL_ACTIONS = {
    UPDATE_VESSELS: 'vessels.update_vessels',
    UPDATE_VESSEL: 'vessels.update_vessel'
}

export const vessels = (vessels, action) => {
    if(!vessels) return [];

    switch(action.type) {
        case VESSEL_ACTIONS.UPDATE_VESSEL:
            return update_vessel(vessels, action);
        case VESSEL_ACTIONS.UPDATE_VESSELS:
            return update_vessels(vessels, action);
        default:
            return vessels;
    }
}

const update_vessel = (vessels, action) => {
    if(!action.vessels) return vessels;
    return vessels
    .slice(0, action.index)
    .concat([action.vessel])
    .concat(vessels.slice(action.index+1))
}

const update_vessels = (vessels, action) => {
    if(!action.vessels) return vessels;
    return action.vessels;
}