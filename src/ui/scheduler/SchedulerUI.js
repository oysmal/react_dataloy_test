import React, { Component } from 'react';
import SchedulerController from './SchedulerController';
import VesselListUI from './vessel-list/VesselListUI';
import { ControlBarUI } from './control-bar/ControlBarUI';

class SchedulerUI extends Component {

    constructor(props) {
        super();
        this.controller = new SchedulerController();

        this.state = {
            vesselsWithVoyages: []
        };

        this.controller.store.subscribe( ()=> {
            this.setState({
                vesselsWithVoyages: this.controller.get_list_of_vessels_with_voyages()
            });
        });
    }

    render() {
        
        return (
            <div>
                <h1>FAS Sample App</h1>
                <ControlBarUI store={this.controller.store}/>
                <VesselListUI vessels={this.state.vesselsWithVoyages} />
            </div>
        );
    }
}

export default SchedulerUI;