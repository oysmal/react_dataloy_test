import React, {Component} from 'react';
import '../FilterUI.css';
import {SelectComponentUI} from '../select-component/SelectComponentUI';

export class FilterVesselUI extends Component {

    constructor(props) {
        super();
        this.props = props;
        this.options = this.props.filterOptions.map(filterOption => {
            return {
                name: filterOption,
                value: filterOption
            };
        });

        this.active = -1;
        this.options.map((option, i) => {
            if(option.name.toLowerCase() == this.props.activeFilter.toLowerCase()) this.active = i;
        });
    }

    render() {
        console.log(this.options);
        return (
            <div className='filter-component'>
                <label>Select vessel types to show: </label>
                <SelectComponentUI options={this.options} selected={this.active} onChange={this.props.onChange} />
            </div>
        )
    }
}

