import React from 'react';
import './SelectComponentUI.css';

export const SelectComponentUI = (props) => {
    return (
        <select className='select-component' onChange={e => {
                    props.onChange(e.target.value);
                }}
                selected={props.selected}>
            {props.options.map(option => {
                return <option name={option.name} value={option.value}>{option.name}</option>
            })}
        </select>
    );
}