import {createFleetPlanStore} from '../../state/rootStore';
import * as VesselGetter from '../../dataloy/actions/vessels/get_vessels';
import * as VoyageGetter from '../../dataloy/actions/voyages/get_voyages';
import {loadAllVessels} from '../../dataloy/actions/vessels/load_vessels';
import {loadAllVoyages} from '../../dataloy/actions/voyages/load_voyages';

class SchedulerController {

    constructor() {
        this.store = createFleetPlanStore();
        loadAllVessels(this.store);
        loadAllVoyages(this.store);
    }

    get_list_of_vessels_with_voyages() {
        console.log(VesselGetter.get_type_filtered_vessels(this.store));
        let vessels = VesselGetter.get_type_filtered_vessels(this.store).map(v => {
            v['voyages'] = [];
            return v;
        });
        let voyages = VoyageGetter.get_type_filtered_voyages(this.store);

         let res = voyages.map( voyage => {
            vessels.map( vessel => {
                if (voyage.vesselRef == vessel.ref) {
                    vessel.voyages.push(voyage);
                }
                return vessel;
            });
        });
        return vessels;
    }

}

export default SchedulerController;