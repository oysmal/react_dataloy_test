import React, { Component } from 'react';
import './VoyageList.css';
import VoyageUI from '../voyage/VoyageUI'

class VoyageListUI extends Component{

    constructor(props) {
        super();
        this.props = props;
    }

    render() {
        return (
            <ul className='voyage-list'>
                {this.props.voyages.map(voyage => {
                    return <VoyageUI voyage={voyage} key={voyage.ref}/>
                })}
            </ul>
        );
    }
}

export default VoyageListUI;