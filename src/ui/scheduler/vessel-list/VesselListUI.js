import React, {Component} from 'react';
import VesselUI from '../vessel/VesselUI';
import './VesselList.css';

class VesselListUI extends Component {

    constructor(props) {
        super();
        this.props = props;
    }

    render() {
        return (
            <ul className='vessel-list'>
                {this.props.vessels.map(vessel => {
                    return <VesselUI vessel={vessel} key={vessel.ref}/>
                })}
            </ul>
        )
    }
}

export default VesselListUI;