import React, {Component} from 'react';
import BackgroundGridUI from '../scheduler-background-grid/BackgroundGridUI';
import VoyageListUI from '../voyage-list/VoyageListUI';
import './Vessel.css';


class VesselUI extends Component {

    constructor(props) {
        super();
        this.props = props;
    }

    render() {
        return (
            <li className='vessel'>
                <div className='vessel-info'>
                    <h4>{this.props.vessel.name}</h4>
                    <p>{this.props.vessel.capacity} DWT</p>
                    <p>Vessel Type: {this.props.vessel.vesselType.split('')[0] + this.props.vessel.vesselType.split('').splice(1).join('').toLowerCase()}</p>
                </div>
                <div className='vessel-voyages'>
                    <VoyageListUI voyages={this.props.vessel.voyages} />
                </div>
                <div className='background-grid-container'>
                     <BackgroundGridUI cols={4} />
                </div>
            </li>
        )
    }
}

export default VesselUI;