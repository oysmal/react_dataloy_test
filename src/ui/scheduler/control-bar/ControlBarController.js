import {VESSEL_FILTERS} from '../../../state/visibility-filter/vessel_filters.js';
import * as VisibilityFilterUpdater from '../../../dataloy/actions/visibility-filter/update_visibility_filters';

export class ControlBarController {
    constructor(store) {
        this.store = store;
    }

    get_vessel_filter_options() {
        return Object.keys(VESSEL_FILTERS);
    }

    get_visibility_filters() {
        return this.store.getState().visibilityFilter;
    }

    on_vessel_type_filter_change(value) {
        VisibilityFilterUpdater.update_vessel_type_filter(this.store, value);
    }
}