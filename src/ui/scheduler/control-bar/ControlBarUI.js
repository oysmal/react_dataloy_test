import React, { Component } from 'react';
import {ControlBarController} from './ControlBarController';
import {FilterVesselUI} from '../filters/vessels/FilterVesselUI';
import {VESSEL_FILTERS} from '../../../state/visibility-filter/vessel_filters.js';
import * as VisibilityFilterUpdater from '../../../dataloy/actions/visibility-filter/update_visibility_filters';
import './ControlBarUI.css';

export class ControlBarUI extends Component {

    constructor(props) {
        super();
        this.props = props;
        this.controller = new ControlBarController(this.props.store);

        this.state = {
            vesselTypeFilterOptions: this.controller.get_vessel_filter_options(),
            visibilityFilter: this.controller.get_visibility_filters()
        };

        this.props.store.subscribe( ()=> {
            this.setState({
                visibilityFilter: this.controller.get_visibility_filters()
            });
        });
    }

    render() {
        
        return (
            <div className='control-bar'>
                <FilterVesselUI 
                    filterOptions={this.state.vesselTypeFilterOptions}
                    activeFilter={this.state.visibilityFilter.vessels}
                    onChange={(value) => this.controller.on_vessel_type_filter_change(value)}/>
            </div>
        );
    }
}

