import React, {Component} from 'react';
import './BackgroundGridUI.css';

class BackgroundGridUI extends Component {

    constructor(props) {
        super();
        this.props = props;
    }

    render() {
        return (
            <div className='vessel-background'>
                {new Array(this.props.cols !== undefined? this.props.cols : 4).fill(0).map((x,i) => {
                    return <div style={{width: `calc(100% / ${this.props.cols} - 2px)`}} key={'vessel.background.'+i}></div>
                })}
            </div>
        )
    }
}

export default BackgroundGridUI;