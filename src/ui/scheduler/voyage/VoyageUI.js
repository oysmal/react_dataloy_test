import React, { Component } from 'react';
import './Voyage.css';

class VoyageUI extends Component{

    constructor(props) {
        super();
        this.props = props;
    }

    render() {
        let widthStyle = {};
        if(this.props.voyage && this.props.voyage.startDate && this.props.voyage.layTarget) {
            let time = this.props.voyage.layTarget.getTime() - this.props.voyage.startDate.getTime();
            console.log(time);
            let widthPercent = parseInt(time/(180*24*60*60*1000)*100);
            widthStyle = {width: widthPercent+'%'};
        }
        console.log(widthStyle);
        let voyage_type_style = {COA:'voyage-type-coa', SPOT:'voyage-type-spot'}[this.props.voyage.type];
        return(
            <li className='voyage-info' style={widthStyle}>
                <div className={voyage_type_style}></div>
                <div className='voyage-info-ports'>
                    <p className='link'>#{this.props.voyage.ref}</p>
                    <p>{this.props.voyage.firstLoadPort} > {this.props.voyage.lastDischargePort}</p>
                    <p>{this.props.voyage.startDate.toISOString().split('T')[0].split('-').join('.')} - {this.props.voyage.layTarget.toISOString().split('T')[0].split('-').join('.')}</p>
                </div>
                <div className='voyage-info-laycan'>
                </div>
            </li>
        );
    }
}

export default VoyageUI;