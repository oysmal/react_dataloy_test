import React, { Component } from 'react';
import SchedulerUI from './ui/scheduler/SchedulerUI';
import logo from './logo.svg';
import './App.css';

class App extends Component {

  static getChildContext() {
    return {
      name: 'test'
    }
  }

  render() {
    return (
      <div className="App">
        <SchedulerUI></SchedulerUI>
      </div>
    );
  }
}

export default App;
