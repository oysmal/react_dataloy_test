export const VOYAGE_TYPES = {
    COA: 'dataloy.models.voyage.coa',
    SPOT: 'dataloy.models.voyage.spot'
}
export class Voyage {
    
    constructor(ref="", type="", charterer="", firstLoadPort="", lastDischargePort="", vesselTypeRequired="", layTarget=null, canTarget=null, startDate=null, vesselRef="") {
        this._ref = ref;
        this._type = type;
        this._charterer = charterer;
        this._firstLoadPort = firstLoadPort;
        this._lastDischargePort = lastDischargePort;
        this._vesselTypeRequired = vesselTypeRequired;
        this._layTarget = layTarget;
        this._canTarget = canTarget;
        this._startDate = startDate;
        this._vesselRef = vesselRef
    }

    get ref() {
        return this._ref;
    }

    get type() {
        return this._type;
    }

    get charterer() {
        return this._charterer;
    }

    get firstLoadPort() {
        return this._firstLoadPort;
    }

    get lastDischargePort() {
        return this._lastDischargePort;
    }
    
    get vesselTypeRequired() {
        return this._vesselTypeRequired;
    }

    get layTarget() {
        return this._layTarget;
    }

    get canTarget() {
        return this._canTarget;
    }

    get startDate() {
        return this._startDate;
    }

    get vesselRef() {
        return this._vesselRef;
    }

    set ref(value) {
        this._ref = value;
    }

    set type(value) {
        this._type = value;
    }

    set charterer(value) {
        this._charterer = value;
    }

    set firstLoadPort(value) {
        this._firstLoadPort = value;
    }

    set lastDischargePort(value) {
        this._lastDischargePort = value;
    }

    set vesselTypeRequired(value) {
        this._vesselTypeRequired = value;
    }

    set layTarget(value) {
        this._layTarget = value;
    }

    set canTarget(value) {
        this._canTarget = value;
    }

    set startDate(value) {
        this._startDate = value;
    }

    set vesselRef(value) {
        this._vesselRef = value;
    }
}