
export const VESSEL_TYPES = {
    CONVENTIONAL: 'CONVENTIONAL',
    CHEMICAL: 'CHEMICAL',
    TANKER: 'TANKER',
    LNG: 'LNG'
}

export class Vessel {

    constructor(ref="", name="", vesselType="", capacity="") {
        this._ref = ref;
        this._name = name;
        this._vesselType = vesselType; 
        this._capacity = capacity
    }

    get ref() {
        return this._ref;
    }

    get name() {
        return this._name;
    }

    get vesselType() {
        return this._vesselType;
    }

    get capacity() {
        return this._capacity;
    }

    set name(value) {
        this._name = value;
    }

    set vesselType(value) {
        this._vesselType = value;
    }

    set capacity(value) {
        this._capacity = value;
    }

    set ref(value) {
        this._ref = value;
    }
}