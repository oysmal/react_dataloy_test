import {Loader} from '../loader';
import {fetch} from '../mock_fetch';

export class VesselLoader extends Loader {
    
    load(url, headers) {
        return fetch(url, headers);
    }
}