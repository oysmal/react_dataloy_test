import {Voyage, VOYAGE_TYPES} from '../models/voyage';
import {Vessel, VESSEL_TYPES} from '../models/vessel';

export const fetch = (url, headers) => {
    return new Promise( (resolve, reject) => {
        if(url.indexOf('voyages') != -1) {
            delayedReturn(resolve, voyages, 1000);
        } else {
            delayedReturn(resolve, vessels, 1000);
        }
    })
}

const delayedReturn = (resolve, data, time) => {
    setTimeout(() => {
        resolve(data);
    }, time);
}


const voyages = [
    new Voyage('3456789', 'COA', 'Bergen Industries', 
        'Amsterdam', 'New York', VESSEL_TYPES.CONVENTIONAL, 
        new Date('2017-11-29T12:00:00'), 
        new Date('2017-11-30T21:00:00'), 
        new Date('2017-10-31T12:00:00'),
        "222222"
    ),
    new Voyage('1234567', 'SPOT', 'Bergen Industries', 
                'Bergen', 'Helsinki', VESSEL_TYPES.CONVENTIONAL, 
                new Date('2017-12-20T08:00:00'), 
                new Date('2018-01-01T20:00:00'), 
                new Date('2017-11-30T12:00:00'),
                "222222"
    ),
    new Voyage('2345678', 'SPOT', 'Chemicals for the Future', 
                'London', 'Shanghai', VESSEL_TYPES.CHEMICAL, 
                new Date('2018-03-15T15:00:00'), 
                new Date('2018-03-17T20:00:00'), 
                new Date('2017-12-25T12:00:00'),
                "333333"
    )
]

const vessels = [
    new Vessel("222222", "MV BERGEN", VESSEL_TYPES.CONVENTIONAL, 20000),
    new Vessel("333333", "MV AURORA", VESSEL_TYPES.CHEMICAL, 25000)
]