import * as VisibilityFilterActionCreators from '../../../state/visibility-filter/visibility_filter_action_creators';

export const update_vessel_type_filter = (store, vessel_types_to_show) => {
    store.dispatch(VisibilityFilterActionCreators.set_vessel_visibility_filter_action(vessel_types_to_show));
}

export const update_voyage_type_filter = (store, voyage_types_to_show) => {
    store.dispatch(VisibilityFilterActionCreators.set_voyage_visibility_filter_action(voyage_types_to_show));
}
