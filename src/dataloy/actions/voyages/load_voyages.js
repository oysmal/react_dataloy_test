import {VoyageLoader} from '../../loaders/voyage/voyage_loader';
import {Voyage} from '../../models/voyage';
import * as VoyageUpdater from './update_voyages';

export const loadAllVoyages = (store) => {
    new VoyageLoader().load('/voyages', {}).then((data) => {
        VoyageUpdater.update_voyages(store, data);
    })
}