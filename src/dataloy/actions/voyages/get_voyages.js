import * as VoyageFilters from '../../utils/filters/filter_voyages';

export const get_type_filtered_voyages = (store) => {
    let state = store.getState();
    return VoyageFilters.filter_voyages_by_type(state.voyages, state.visibilityFilter.voyages);
}