import * as VoyageActionCreators from '../../../state/voyages/voyage_action_creators';

export const update_voyage = (store, voyage, index) => {
    store.dispatch(VoyageActionCreators.update_voyage_action(voyage, index));
}

export const update_voyages = (store, voyages) => {
    store.dispatch(VoyageActionCreators.update_voyages_action(voyages));
}