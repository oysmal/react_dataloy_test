import * as VesselFilters from '../../utils/filters/filter_vessels';

export const get_type_filtered_vessels = (store) => {
    let state = store.getState();
    
    return VesselFilters.filter_vessels_by_type(state.vessels, state.visibilityFilter.vessels);
}