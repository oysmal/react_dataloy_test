import {VesselLoader} from '../../loaders/vessel/vessel_loader';
import {Vessel} from '../../models/vessel';
import * as VesselUpdater from './update_vessels';

export const loadAllVessels = (store) => {
    new VesselLoader().load('/vessels', {}).then((data) => {
        VesselUpdater.update_vessels(store, data);
    });
}