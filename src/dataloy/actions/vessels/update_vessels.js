import * as VesselActionCreators from '../../../state/vessels/vessel_action_creators';

export const update_vessel = (store, vessel, index) => {
    store.dispatch(VesselActionCreators.update_vessel_action(vessel, index));
}

export const update_vessels = (store, vessels) => {
    store.dispatch(VesselActionCreators.update_vessels_action(vessels));
}