export const SORT_CONSTANTS = {
    ASC: 1,
    DESC: -1
}

export const sort_vessels_by_name = (vessels, direction) => {
    vessels = vessels.sort((a, b)=> {
        if (direction == SORT_CONSTANTS.ASC) {
            a.vessel_name < b.vessel_name;
        } else {
            a.vessel_name > b.vessel_name;
        }
    });
}

export const sort_vessels_by_dwt = (vessels, direction) => {
    vessels = vessels.sort((a, b)=> {
        if (direction == SORT_CONSTANTS.ASC) {
            a.weight < b.weight;
        } else {
            a.weight > b.weight;
        }
    });
}