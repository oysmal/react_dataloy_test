import {VESSEL_FILTERS} from '../../../state/visibility-filter/vessel_filters';

export const filter_vessels_by_type = (vessels, filter) => {
    if (filter == VESSEL_FILTERS.ALL) return vessels;
    return vessels.filter( vessel => {
        console.log(vessel.vesselType);
        console.log(filter);
        if (vessel.vesselType.toLowerCase() == filter.toLowerCase()) return vessel;
    });
}

export const filter_vessels_by_contains_name = (vessels, name) => {
    return vessels.filter( vessel => {
        if (vessel.name.indexOf(name) != -1) return vessel;
    });
}