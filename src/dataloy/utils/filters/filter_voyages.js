import {VOYAGE_FILTERS} from '../../../state/visibility-filter/voyage_filters';

export const filter_voyages_by_type = (voyages, filter) => {
    if (filter == VOYAGE_FILTERS.ALL) return voyages;

    return voyages.filter( voyage => {
        if (voyage.type.toLowerCase() == filter.toLowerCase()) return voyage;
    });
}